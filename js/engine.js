$(function() {


// Banner-slider
var bannerSlider =  $('.banner-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    fade: true,
    pauseOnHover: false,
    arrows: false,
    responsive: [
    {
      breakpoint: 767,
      settings: {
        dots: true
      }
    }
  ]
});

$('.banner-slider__prev').click(function(){
  $(bannerSlider).slick("slickPrev");
});
$('.banner-slider__next').click(function(){
  $(bannerSlider).slick("slickNext");
});

// News-slider
$('.news-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    arrows: false,
    responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
      }
    }
  ]
});

// popular-slider
$('.popular-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    arrows: false,
    responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
      }
    }
  ]
});

// popular-slider
$('.product-related__slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    arrows: false,
    responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
      }
    }
  ]
});





// Выпадающее меню
$(".navbar-toggle").on("click", function () {
    $(this).toggleClass("active");
    $('.nav.navbar-nav').fadeToggle()
});

if($(window).width() < 768){
  $('.toggler').click(function() {
    $(this).toggleClass('active');
    $(this).parent('.dropdown').find("ul").stop(true,true).fadeToggle();
  })


}


// Меню каталога


  $('.dropdown > a').click(function() {
    $(this).next('.dropdown-menu').fadeToggle();
    $(this).parent('.dropdown').toggleClass('active');
  })


$('.close-menu__btn, .btn-cetegory').click(function() {
    $('.catalog-menu').fadeToggle()
})




// Табы
$('.tabs-nav a, .tabs-mobile__btn').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $(this).parents('.tabs-wrapper').find('#' + _id);
    $('.tabs-nav a').removeClass('active');
    $(this).addClass('active');
    $('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;
    

});

$('.tabs-mobile__btn').click(function() {
    $('.tabs-mobile__item').addClass('active');
   $(this).parent('.tabs-mobile__item').removeClass('active')
    return false;

});

// Стилизация селектов
$('select').styler();



// FansyBox 2
 $('.fancybox').fancybox();



// Открыть\закрыть вход для партнеров
$('.login-btn').click(function() {
  $(this).parent('.login').toggleClass('active')
})



// Движение блока .sidebar-right за курсором
 window.onload = function() { 
      $('.airSticky').airStickyBlock();
  };






})